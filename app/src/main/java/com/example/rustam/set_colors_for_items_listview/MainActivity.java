package com.example.rustam.set_colors_for_items_listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private String[] names={"blue","deeppink","green","yellow","red","cyan","orange"};
    private List<Data> Base;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Base=load();
        ListView listView=(ListView) findViewById(R.id.listview);
        Adapter adapter=new Adapter(Base);
        listView.setAdapter(adapter);
    }

    public List<Data> load()
    {
        List<Data> t= new ArrayList<Data>();
        for(int i=0; i<100; i++)
        {
            t.add(new Data(names[i%7]));
        }
        return t;
    }
}
