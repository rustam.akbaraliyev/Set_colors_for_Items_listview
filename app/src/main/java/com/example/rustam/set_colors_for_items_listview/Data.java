package com.example.rustam.set_colors_for_items_listview;

/**
 * Created by Rustam on 23.02.2017.
 */

public class Data {
    String Name;

    public Data(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
