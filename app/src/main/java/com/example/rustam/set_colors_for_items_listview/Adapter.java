package com.example.rustam.set_colors_for_items_listview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Rustam on 23.02.2017.
 */

public class Adapter extends BaseAdapter {
    private List<Data> MyData;
    int count=0;
    private String[] names={"blue","deeppink","green","yellow","red","cyan","orange"};
    //drawabledan ranglarni massivga joylashtirish
    private int[] colors={R.drawable.blue,R.drawable.deeppink,R.drawable.green,R.drawable.yellow,R.drawable.red,R.drawable.cyan,R.drawable.orange};

    public Adapter(List<Data> myData) {
        MyData = myData;
    }

    @Override
    public int getCount() {

        return MyData.size();
    }

    @Override
    public Object getItem(int i) {
        return MyData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Data t=MyData.get(i);

        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item,viewGroup,false);
        TextView textView=(TextView) view.findViewById(R.id.name);
        //quyidagi ranglarni nomi
        textView.setText(names[count%7]);
        //rangni quydagicha bersak ham buladi textView.setText(t.getName());
        // lekin bunda rangnomi bilan rang mos tushmay qoladi sababini keyinchalik bilib olasiz
        //quyida item mizga backgroundiga rang joylashtirish
        view.setBackgroundResource(colors[count%7]);
        count++;
        return view;
    }
}
